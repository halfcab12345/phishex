# PHISHEX - Lightweight Phishing Web Scanner for HTML/Emails
![Phishex.co](https://i.imgur.com/cRN7QBZ.png "Phishex.co")


###Features

- Supports text files
- 6 million phishing links and growing [[phishtank.com API](http://phishtank.com "phishtank.com API")]
- Fast results
- Automatically checks all links of an html file

###In Development
###Oct. 7th 2018

------------
This project is being developed as a Capstone requirement for graduation at University in the Bachelors of Science: Software Development program. It scans HTML documents for phishing links.

------------

