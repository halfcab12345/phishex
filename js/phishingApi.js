const checkUrl = function(url){
    const phishTankApi = 'https://cors-escape.herokuapp.com/http://checkurl.phishtank.com/checkurl/'
    const app_key = '61c76b55c9f2adcad1605eab071733b7009992b5f7d4264d587fccfca57461e7'
    const format = 'json'
    let phishData = {
        errortext: '',
        status: '',
        info: '',
        url: url,
        valid: '',
        verified: '',
        discovered: ''
    }

    $.post(phishTankApi,{url: url, app_key: app_key, format: format})
        .done(function(data){

            phishData.errortext = data.errortext || 'n/a'
            phishData.status = data.meta.status || 'n/a'
            phishData.in_database = data.results ? data.results.in_database : 'n/a'
            phishData.info = data.results ? data.results.phish_detail_page : 'n/a'
            phishData.valid = data.results ? data.results.valid : 'n/a'
            phishData.verified = data.results ? data.results.verified : 'n/a'
            phishData.verified_at = data.results? data.results.verified_at : 'n/a'

           $('#data').append(
               `<tr>
                <td>${phishData.url}</td>
                <td>${phishData.errortext}</td>
                <td>${phishData.status}</td>
                <td>${phishData.in_database}</td>
                <td>${phishData.info}</td>
                <td>${phishData.valid}</td>
                <td>${phishData.verified}</td>
                <td>${phishData.verified_at}</td>
               </tr>`)
        })


}




