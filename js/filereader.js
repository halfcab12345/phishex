let unique

const openFile = function(uploadedFile){

    let file = new FileReader()

    file.onload = function(event) {

        const rawHTML = event.target.result

        const doc = document.createElement("html");
        doc.innerHTML = rawHTML;
        const links = doc.getElementsByTagName("a")
        let urls = [];

        for (var i=0; i<links.length; i++) {
            let url = links[i].getAttribute("href")
            if (url.indexOf('http') !== -1 || url.indexOf('https') !== -1){
                urls.push(url);
            }
        }

        const unique = [...new Set(urls)]

        if(unique.length){
            checkAll(unique)
        } else {
            $('#data').append(`<tr></tr><td>No Potential Phishing Links</td></tr>`)
        }


    }

    file.readAsText(uploadedFile)

}



